from django.contrib import admin
from .models import TransactionModel


@admin.register(TransactionModel)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id',]
    search_fields = ['id',]