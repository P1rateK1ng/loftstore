from django.db import models
from orders.models import Order
from utils.basemodels import BaseModel


class TransactionModel(BaseModel):
    TRANSACTION_STATUS = (
        ('completed', 'завершена'),
        ('sent', 'отправлена'),
        ('incompleted', 'в ожидании'),
        ('declined', 'отклонена'),
        ('denied', 'отказано'),
        ('expired', 'просроченно'),
    )

    payment_id = models.CharField(max_length=1000, verbose_name='id транзакции')
    payment_status = models.CharField(max_length=11, choices=TRANSACTION_STATUS, blank=True, default='not completed')
    payment_type = models.CharField(max_length=100, verbose_name='тип транзакции')
    payment_info = models.CharField(max_length = 10000, null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, verbose_name='заказ транзакции', related_name='order')
    value = models.DecimalField(max_digits=6, decimal_places=2, verbose_name='сумма')

    class Meta:
        verbose_name = 'транзакция'
        verbose_name_plural = 'транзакции'
        ordering = ('-created_at',)

    def __str__(self):
        return self.id.hex