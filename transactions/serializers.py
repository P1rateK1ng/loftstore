from rest_framework import serializers
from transactions.models import TransactionModel

# class OrderSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Order
#         fields = ('id', 'total_cost')
#
# class ListTransactionSerializer(serializers.ModelSerializer):
#     order = OrderSerializer()
#     class Meta:
#         model = TransactionModel
#         fields = '__all__'
#
# class CreateTransactionSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = TransactionModel
#         fields = '__all__'

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionModel
        fields = '__all__'

