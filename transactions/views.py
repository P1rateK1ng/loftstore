from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from transactions.models import TransactionModel
from transactions import serializers


class TransactionView(viewsets.ModelViewSet):
    queryset = TransactionModel.objects.all()
    permission_classes = (IsAdminUser,)
    serializer_class = serializers.TransactionSerializer

#   def get_serializer_class(self):
#        if self.action == 'list':
#           return serializers.ListTransactionSerializer
#        if self.action == 'create':
#            return serializers.CreateTransactionSerializer
#        if self.action == 'retrieve':
#            return serializers.ListTransactionSerializer
