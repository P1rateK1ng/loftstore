from django.urls import path, include
from rest_framework.routers import DefaultRouter
from transactions.views import TransactionView

app_name = 'transactions'
router = DefaultRouter()
router.register(r'', TransactionView)

urlpatterns = [
    path('', include(router.urls)),
    path('<str:pk>', TransactionView.as_view({'get': 'retrieve'}), name="transaction-retrieve"),
]

