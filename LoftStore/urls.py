from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView


urlpatterns = [
    # Swagger documentation
    path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
    # Optional UI:
    path('api/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('api/schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),

    path('admin/', admin.site.urls),
    # For testing the API from browser
    path('session-auth/', include('rest_framework.urls')),

    # My app urls
    path('products/', include('products.urls'), name='products-app'),
    path('users/', include('users.urls'), name='users-app'),
    path('orders/', include('orders.urls'), name='orders-app'),
    path("carts/", include('carts.urls'), name='carts-app'),
    path("transactions/", include('transactions.urls'), name='transactions-app'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)