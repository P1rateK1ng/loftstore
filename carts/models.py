from django.db import models

from products.models import ProductModel
from users.models import UserModel
from utils.basemodels import BaseModel


class CartModel(BaseModel):

    def __str__(self):
        return self.id.hex

    class Meta:
        verbose_name = "Корзина"
        verbose_name_plural = "Корзины"

class CartItemModel(models.Model):
    cart = models.ForeignKey(CartModel, on_delete=models.CASCADE, verbose_name='корзина', related_name='items')
    user = models.ForeignKey(UserModel, on_delete=models.CASCADE, verbose_name='покупатель', related_name='user')
    product = models.ForeignKey(ProductModel, on_delete=models.CASCADE, verbose_name='продукт', related_name='products')
    count = models.PositiveIntegerField(default=0, verbose_name='количество продукта в корзине')

    def __str__(self):
        return self.id.hex

    class Meta:
        verbose_name = "Товар в корзине"
        verbose_name_plural = "Товары в корзине"