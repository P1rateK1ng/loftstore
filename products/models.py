from autoslug import AutoSlugField
from django.db import models
from slugify import slugify

from utils.basemodels import BaseModel


def set_slugify(value) -> str:
    return slugify(value).replace(' ', '-')


class ProductCategoryModel(BaseModel):
    name = models.CharField(max_length=255, verbose_name='Категория')
    slug = AutoSlugField(populate_from='name', slugify=set_slugify, verbose_name='slug')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class ProductModel(BaseModel):
    name = models.CharField(verbose_name='Наименование', max_length=1000)
    slug = AutoSlugField(populate_from='name', slugify=set_slugify, verbose_name='slug')
    category = models.ForeignKey(ProductCategoryModel,verbose_name='Категория', on_delete=models.PROTECT, related_name='products')
    description = models.TextField(verbose_name='Описание', max_length=20000)
    price = models.DecimalField(verbose_name='Цена', max_digits=8, decimal_places=2)
    count = models.IntegerField(verbose_name='Количество')


    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name

class ProductImageModel(BaseModel):
    product = models.ForeignKey(ProductModel, verbose_name='продукт', on_delete=models.CASCADE, related_name = "images")
    image = models.ImageField(upload_to='product_images/%Y-%m-%d', verbose_name='Картинки продукта')

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Фото товара'
        verbose_name_plural = 'Фото товаров'