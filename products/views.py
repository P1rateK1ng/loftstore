from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.viewsets import ModelViewSet
from products.serializers import ProductSerializer, ProductCategorySerializer
from products.models import ProductModel, ProductCategoryModel
from utils.permissions import IsAdminOrReadOnly


class ProductCategoryViews(ModelViewSet):
    queryset = ProductCategoryModel.objects.all()
    serializer_class = ProductCategorySerializer
    permission_classes = (IsAdminOrReadOnly,)


class ProductViewSet(ModelViewSet):
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAdminOrReadOnly,)