from django.urls import path
from rest_framework.routers import SimpleRouter

from products.views import ProductViewSet, ProductCategoryViews

app_name = 'products'
router = SimpleRouter()
router.register(r'', ProductViewSet, basename="product")
# router.register(r'category', ProductCategoryViews, basename="category")

urlpatterns = [
    path("category/", ProductCategoryViews.as_view({"get":"list"}), name="product_category_list_view"),
    path("category/create/", ProductCategoryViews.as_view({"post":"create"}), name="product_category_create"),
]
urlpatterns += router.urls
#
# for i in router.urls:
#     print (i, '\n')
