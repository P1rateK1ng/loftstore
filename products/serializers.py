from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
from products.models import ProductModel, ProductCategoryModel, ProductImageModel
from drf_writable_nested import WritableNestedModelSerializer


class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategoryModel
        fields = "__all__"


class ProductImageSerializer(WritableNestedModelSerializer, serializers.ModelSerializer):
    image = Base64ImageField()
    class Meta:
        model = ProductImageModel
        exclude = ["id", "product"]

class ProductSerializer(WritableNestedModelSerializer, serializers.ModelSerializer):
    images = ProductImageSerializer(many=True, required=True)

    # uploaded_images = serializers.ListField(child=serializers.ImageField(max_length=1000000, allow_empty_file=False, use_url=False), write_only=True, required=False)

    class Meta:
        model = ProductModel
        fields = ["id", "name", "category", "description", "price", "count", "images"]

    # def create(self, validated_data):
    #     uploaded_images = validated_data.pop("uploaded_images")
    #     product = ProductModel.objects.create(**validated_data)
    #     for image in uploaded_images:
    #         ProductImageModel.objects.create(product=product, image=image)
    #     return product
