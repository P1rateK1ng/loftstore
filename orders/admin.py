from django.contrib import admin
from orders.models import Order, OrderedProduct

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'created_at', 'total_cost',)
    search_fields = ('user',)
    list_filter = ('created_at',)
    empty_value_display = '-пусто-'


@admin.register(OrderedProduct)
class OrderedProductAdmin(admin.ModelAdmin):
    list_display = ('order', 'product', 'product_quantity',)
    search_fields = ('product',)
    empty_value_display = '-пусто-'