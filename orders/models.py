from django.db import models

from users.models import UserModel
from products.models import ProductModel
from utils.basemodels import BaseModel


class Order(BaseModel):

    ORDER_STATUS = (
        ('new', 'Новый'),
        ('paid', 'Оплачен'),
        ('sent', 'Отправлен'),
        ('completed', 'Завершен'),
        ('canceled', 'Отменён')
    )

    status = models.CharField(max_length=9, choices=ORDER_STATUS, blank=True, default='new')
    user = models.ForeignKey(UserModel, on_delete=models.CASCADE, verbose_name='Заказавший пользователь')
    total_cost = models.DecimalField(verbose_name='Сумма заказа', max_digits=9, decimal_places=2)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return self.id.hex


class OrderedProduct(BaseModel):
    order = models.ForeignKey(Order, verbose_name='id заказа', on_delete=models.CASCADE)
    product = models.ForeignKey(ProductModel, verbose_name='id товара',  on_delete=models.CASCADE)
    product_quantity = models.IntegerField(verbose_name='количество заказанных товаров')
    product_price = models.DecimalField(verbose_name='Цена товара на момент заказа', max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = 'Заказанный товар'
        verbose_name_plural = 'Заказанные товары'

    def __str__(self):
        return self.id.hex
