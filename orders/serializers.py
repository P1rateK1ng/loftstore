from rest_framework.serializers import ModelSerializer
from orders.models import Order, OrderedProduct

class OrderedProductSerializer(ModelSerializer):
    class Meta:
        model = OrderedProduct
        fields = ('__all__')

class OrderSerializer(ModelSerializer):
    class Meta:
        model = Order
        fields = ('__all__')
