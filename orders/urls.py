from django.urls import path, include
from orders.views import OrderViewSet, OrderedProductViewSet
from rest_framework import routers

app_name = 'orders'

router = routers.DefaultRouter()
router.register(r'orders', OrderViewSet, basename='orders')
router.register(r'items', OrderedProductViewSet, basename='items')

urlpatterns = [
    path('', include(router.urls))
]