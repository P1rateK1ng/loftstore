from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView
from users.views import UserViewSet, MyObtainTokenPairView, RegisterView, ChangePasswordView

urlpatterns = [
    path('list/', UserViewSet.as_view({'get': 'list'}), name='api-UserViewSet'), #Get request only
    path('token/', MyObtainTokenPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('register/', RegisterView.as_view(), name='auth_register'),
    path('change-password/<str:pk>', ChangePasswordView.as_view(), name='user_change_password'),
]