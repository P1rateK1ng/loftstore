from rest_framework import viewsets, generics, status
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView
from users.models import UserModel
from users.serializers import UserSerializer, MyTokenObtainPairSerializer, RegisterSerializer, ChangePasswordSerializer
# from users.services import update_password


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    #   Only admin user can see the list of users But doesn't have access to update the info of users.
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)


class MyObtainTokenPairView(TokenObtainPairView):
    #   It's for getting the JWT token.
    serializer_class = MyTokenObtainPairSerializer
    permission_classes = (AllowAny,)


class RegisterView(generics.CreateAPIView):
    #   Registration view, anyone has access to it.
    queryset = UserModel.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        refresh = RefreshToken.for_user(user)
        data = {
            'email': user.email,
            'access_token': str(refresh.access_token),
        }
        return Response(data, status=status.HTTP_201_CREATED)

class ChangePasswordView(generics.UpdateAPIView):
    #   Password changing view.
    serializer_class = ChangePasswordSerializer
    queryset = UserModel.objects.all()
    permission_classes = (IsAuthenticated,)

